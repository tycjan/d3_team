package pl.d3_team.api.repository

import org.springframework.data.jpa.repository.JpaRepository
import pl.d3_team.api.domain.model.entity.Admin

interface AdminRepository : JpaRepository<Admin, String> {

    fun findByEmail(email: String): Admin?

    fun findByAuthToken(token: String): Admin?
}