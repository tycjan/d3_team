package pl.d3_team.api.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import pl.d3_team.api.domain.model.entity.Window

@Repository
interface WindowRepository : JpaRepository<Window, String> {

    fun findAllByShopIdAndFreeIsTrue(shopId: String): List<Window>

    fun findAllByShopId(shopId: String): List<Window>
}