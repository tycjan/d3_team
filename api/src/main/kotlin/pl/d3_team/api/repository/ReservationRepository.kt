package pl.d3_team.api.repository

import org.springframework.data.jpa.repository.JpaRepository
import pl.d3_team.api.domain.model.entity.Reservation

interface ReservationRepository : JpaRepository<Reservation, String> {

    fun findBySecret(secret: String): Reservation?

    fun findByShopIdAndCode(shopId: String, code: Int): Reservation?

    fun findAllByVerifiedIsFalse(): List<Reservation>

    fun findByEmailAndShopId(email: String, shopId: String): Reservation?
}