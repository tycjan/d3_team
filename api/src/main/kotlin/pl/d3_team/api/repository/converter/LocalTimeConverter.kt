package pl.d3_team.api.repository.converter

import java.sql.Time
import java.time.LocalTime
import javax.persistence.AttributeConverter
import javax.persistence.Converter

@Converter(autoApply = true)
class LocalTimeConverter: AttributeConverter<LocalTime, Time> {

    override fun convertToDatabaseColumn(localTime: LocalTime?): Time? =
            localTime?.let { Time.valueOf(localTime) }

    override fun convertToEntityAttribute(dbData: Time?): LocalTime? =
            dbData?.let{ dbData.toLocalTime() }
}