package pl.d3_team.api.repository

import org.springframework.data.jpa.repository.JpaRepository
import pl.d3_team.api.domain.model.entity.Shop

interface ShopRepository : JpaRepository<Shop, String> {

    fun findByOwnerId(ownerId: String): Shop?
}