package pl.d3_team.api.domain.model.entity

import java.io.Serializable
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "shop")
class Shop (
        @Id
        @Column(name = "id", nullable = false, unique = true, length = 250)
        val id: String = UUID.randomUUID().toString(),

        @Column(name = "name", nullable = false, length = 250)
        val name: String,

        @Column(name = "address", nullable = false, length = 250)
        val address: String,

        @Column(name = "openFrom", nullable = false)
        val openFrom: Int,

        @Column(name = "openTo", nullable = false)
        val openTo: Int,

        @Column(name = "shopInterval", nullable = false)
        val shopInterval: Int,

        @Column(name = "maxClient", nullable = false)
        val maxClient: Int,

        @Column(name = "ownerId", nullable = false, unique = true, length = 250)
        val ownerId: String
)