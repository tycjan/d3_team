package pl.d3_team.api.domain.service

import com.mailjet.client.ClientOptions
import com.mailjet.client.MailjetClient
import com.mailjet.client.MailjetRequest
import com.mailjet.client.resource.Emailv31
import org.json.JSONArray
import org.json.JSONObject
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import pl.d3_team.api.domain.model.entity.Reservation

@Component
class MailService(
        @Value("\${mail.key.public}")
        private val mailjetPublicKey: String,
        @Value("\${mail.key.private}")
        private val mailjetPrivateKey: String,
        @Value("\${mail.sender}")
        private val mailjetSenderMail: String,
        @Value("\${mail.verify.templateId}")
        private val mailjetVerifyTemplateId: Int
) {
    private val client: MailjetClient =
            MailjetClient(mailjetPublicKey, mailjetPrivateKey, ClientOptions("v3.1"))

    fun sendVerificationMail(reservation: Reservation) {
        val request = MailjetRequest(Emailv31.resource)
                .property(Emailv31.MESSAGES, JSONArray()
                        .put(JSONObject()
                                .put(Emailv31.Message.FROM, JSONObject()
                                        .put("Email", mailjetSenderMail)
                                        .put("Name", "Safe-Shopping"))
                                .put(Emailv31.Message.TO, JSONArray()
                                        .put(JSONObject().put("Email", reservation.email)))
                                .put(Emailv31.Message.SUBJECT, "Safe-Shopping reservation verification")
                                .put(Emailv31.Message.TEMPLATEID, mailjetVerifyTemplateId)
                                .put(Emailv31.Message.TEMPLATELANGUAGE, true)
                                .put(Emailv31.Message.VARIABLES, JSONObject()
                                        .put("reservation_secret", verifyReservationPath(reservation.secret)))))

        val response = client.post(request)
        println("response: ${response.status}")
        println("responsedata: ${response.data}")
    }

    fun verifyReservationPath(secret: String) =
            ServletUriComponentsBuilder.fromCurrentContextPath()
                    .path("/v1/reservation/verify/")
                    .path(secret)
                    .toUriString()
}