package pl.d3_team.api.domain.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Component
import pl.d3_team.api.application.model.request.ShopCreateRequest
import pl.d3_team.api.domain.model.entity.Admin
import pl.d3_team.api.domain.model.entity.Shop
import pl.d3_team.api.domain.model.exception.ConflictException
import pl.d3_team.api.repository.ShopRepository

@Component
class ShopService {

    @Autowired
    private lateinit var shopRepository: ShopRepository

    @Autowired
    private lateinit var windowService: WindowService

    fun postShop(req: ShopCreateRequest, owner: Admin): Shop {
        if(shopRepository.findByOwnerId(owner.id) != null) throw ConflictException()

        val shop = Shop(
                name = req.name,
                address = req.address,
                maxClient = req.maxClient,
                openFrom = req.openFrom,
                openTo = req.openTo,
                shopInterval = req.shopInterval,
                ownerId = owner.id
        )

        val savedShop = shopRepository.save(shop)

        windowService.createWindows(shop)

        return savedShop
    }

    fun getShop(owner: Admin): Shop? =
            shopRepository.findByOwnerId(owner.id)

    fun getAllShops(): List<Shop> =
            shopRepository.findAll()

    fun getById(id: String): Shop? =
            shopRepository.findByIdOrNull(id)

}