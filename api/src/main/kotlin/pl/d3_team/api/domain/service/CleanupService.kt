package pl.d3_team.api.domain.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import pl.d3_team.api.domain.model.exception.UnauthorizedException
import pl.d3_team.api.repository.ReservationRepository
import pl.d3_team.api.repository.WindowRepository

@Component
class CleanupService {

    @Autowired
    private lateinit var reservationRepository: ReservationRepository
    @Autowired
    private lateinit var windowRepository: WindowRepository

    fun cleanup() {
        println("cleanup issued")
        reservationRepository.deleteAll()
        val windows = windowRepository.findAll()
        windows.forEach {
            it.free = true
            it.freeCount = it.maxClient
        }
        windowRepository.saveAll(windows)
    }
}