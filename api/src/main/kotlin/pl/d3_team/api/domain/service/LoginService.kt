package pl.d3_team.api.domain.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import pl.d3_team.api.domain.security.PasswordEncoder
import pl.d3_team.api.domain.model.entity.Admin
import pl.d3_team.api.domain.model.exception.UnauthorizedException
import pl.d3_team.api.repository.AdminRepository

@Component
class LoginService {

    @Autowired
    private lateinit var adminRepository: AdminRepository
    @Autowired
    private lateinit var passwordEncoder: PasswordEncoder

    fun login(email: String, password: String): String {
        val admin = adminRepository.findByEmail(email) ?: throw UnauthorizedException()

        if (!passwordEncoder.isValid(password, admin.password)) {
            throw UnauthorizedException()
        }

        return admin.authToken
    }

    fun getByAuthToken(token: String): Admin? =
            adminRepository.findByAuthToken(token)
}