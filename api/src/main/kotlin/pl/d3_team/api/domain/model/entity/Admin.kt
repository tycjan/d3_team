package pl.d3_team.api.domain.model.entity

import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "admin")
class Admin(
        @Id
        @Column(name = "id", nullable = false, unique = true, length = 250)
        val id: String = UUID.randomUUID().toString(),

        @Column(name = "email", nullable = false, unique = true, length = 250)
        val email: String,

        @Column(name = "password", nullable = false, unique = false,length = 250)
        val password: String,

        @Column(name = "authToken", nullable = false, unique = true, length = 250)
        val authToken: String = UUID.randomUUID().toString()
)