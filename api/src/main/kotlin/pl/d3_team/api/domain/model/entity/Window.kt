package pl.d3_team.api.domain.model.entity

import pl.d3_team.api.domain.model.exception.ConflictException
import java.time.LocalTime
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "window")
data class Window(
        @Id
        @Column(name = "id", nullable = false, unique = true, length = 250)
        val id: String = UUID.randomUUID().toString(),

        @Column(name = "fromTime", nullable = true)
        val from: LocalTime,

        @Column(name = "toTime", nullable = false)
        val to: LocalTime,

        @Column(name = "free", nullable = false)
        var free: Boolean = true,

        @Column(name = "maxClient", nullable = false)
        var maxClient: Int,

        @Column(name = "freeCount", nullable = false)
        var freeCount: Int,

        @Column(name = "shopId", nullable = false, length = 250)
        val shopId: String
) {

        fun decrementFreeCount() {
                if(free) {
                        freeCount -= 1

                        if (freeCount == 0) {
                                free = false
                        }
                } else {
                        throw ConflictException()
                }
        }

        fun incrementFreeCount() {
                if (freeCount < maxClient) {
                        if (freeCount == 0) {
                                free = true
                        }

                        freeCount += 1
                }
        }
}