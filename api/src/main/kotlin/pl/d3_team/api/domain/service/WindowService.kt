package pl.d3_team.api.domain.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Component
import pl.d3_team.api.domain.model.entity.Shop
import pl.d3_team.api.domain.model.entity.Window
import pl.d3_team.api.domain.model.exception.NotFoundException
import pl.d3_team.api.repository.ShopRepository
import pl.d3_team.api.repository.WindowRepository
import java.time.LocalTime

@Component
class WindowService {

    @Autowired
    private lateinit var windowRepository: WindowRepository
    @Autowired
    private lateinit var shopRepository: ShopRepository

    fun getByShopIdAndFree(shopId: String): List<Window> {
        shopRepository.findByIdOrNull(shopId) ?: throw NotFoundException()

        val now = LocalTime.now()

        return windowRepository.findAllByShopIdAndFreeIsTrue(shopId)
                .filter { it.from.isAfter(now) }
    }

    fun getByShopId(shopId: String): List<Window> =
            windowRepository.findAllByShopId(shopId)

    fun createWindows(shop: Shop) {
        val firstWindowTime = LocalTime.of(shop.openFrom, 0)
        val lastWindowTime = LocalTime.of(shop.openTo, 0)
        val shopInterval = shop.shopInterval.toLong()

        var windowTimeFrom = firstWindowTime
        while (windowTimeFrom.plusMinutes(shopInterval).isBefore(lastWindowTime)) {
            val windowTimeTo = windowTimeFrom.plusMinutes(shopInterval)

            createSingleWindow(windowTimeFrom, windowTimeTo, shop)
            windowTimeFrom = windowTimeTo
        }
    }

    private fun createSingleWindow(timeFrom: LocalTime, timeTo: LocalTime, shop: Shop) {
        val window = Window(
                from = timeFrom,
                to = timeTo,
                maxClient = shop.maxClient,
                freeCount = shop.maxClient,
                free = true,
                shopId = shop.id
        )

        windowRepository.save(window)
    }

    fun getById(id: String): Window? =
            windowRepository.findByIdOrNull(id)
}