package pl.d3_team.api.domain.security

import at.favre.lib.crypto.bcrypt.BCrypt
import org.springframework.stereotype.Component

@Component
class PasswordEncoder {

    fun isValid(password: String, hash: String) =
        BCrypt.verifyer()
            .verify(password.toCharArray(), hash.toCharArray()).verified
}