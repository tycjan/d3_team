package pl.d3_team.api.domain.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Component
import pl.d3_team.api.application.model.request.ReservationCreateRequest
import pl.d3_team.api.domain.model.entity.Reservation
import pl.d3_team.api.domain.model.entity.Shop
import pl.d3_team.api.domain.model.exception.BadRequestException
import pl.d3_team.api.domain.model.exception.ConflictException
import pl.d3_team.api.domain.model.exception.NotFoundException
import pl.d3_team.api.repository.ReservationRepository
import pl.d3_team.api.repository.WindowRepository
import java.time.LocalTime
import java.util.*

@Component
class ReservationService {

    @Autowired
    private lateinit var reservationRepository: ReservationRepository
    @Autowired
    private lateinit var windowRepository: WindowRepository
    @Autowired
    private lateinit var mailService: MailService

    fun createReservation(req: ReservationCreateRequest): Reservation {
        val window = windowRepository.findByIdOrNull(req.windowId) ?: throw BadRequestException()

        if(window.from.isBefore(LocalTime.now())) {
            throw BadRequestException()
        }

        val reservation = Reservation(
                email = req.email,
                windowId = window.id,
                issuedAt = Date(),
                shopId = window.shopId
        )

        reservationRepository.findByEmailAndShopId(reservation.email, reservation.shopId)?.let{
            throw ConflictException()
        }

        window.decrementFreeCount()

        val savedReservation = reservationRepository.save(reservation)
        windowRepository.save(window)

        runCatching {
            mailService.sendVerificationMail(savedReservation)
        }

        return savedReservation
    }

    fun verify(secret: String): Reservation {
        val reservation = reservationRepository.findBySecret(secret) ?: throw NotFoundException()

        val updatedReservation = reservation.copy(verified = true)

        return reservationRepository.save(updatedReservation)
    }

    fun getByShopAndCode(shop: Shop, code: Int): Reservation {
        return reservationRepository.findByShopIdAndCode(shop.id, code) ?: throw NotFoundException()
    }

    fun removeUnverifiedReservations() {
        val tenMinutesAgo = Date(Date().time - 10 * 60 * 1000)

        reservationRepository.findAllByVerifiedIsFalse().forEach {
            if(it.issuedAt.before(tenMinutesAgo)) {
                reservationRepository.delete(it)

                windowRepository.findByIdOrNull(it.windowId)?.let{ window ->
                    window.incrementFreeCount()
                    windowRepository.save(window)
                }
            }
        }
    }

    fun getById(id: String): Reservation? =
            reservationRepository.findByIdOrNull(id)
}