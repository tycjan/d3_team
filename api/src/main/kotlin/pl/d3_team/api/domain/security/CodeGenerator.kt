package pl.d3_team.api.domain.security

object CodeGenerator {
    fun generate(): Int = (1000 .. 9999).random()
}