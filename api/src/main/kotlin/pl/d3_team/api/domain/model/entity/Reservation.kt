package pl.d3_team.api.domain.model.entity

import pl.d3_team.api.domain.security.CodeGenerator
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "reservation")
data class Reservation(
        @Id
        @Column(name = "id", nullable = false, unique = true, length = 250)
        val id: String = UUID.randomUUID().toString(),

        @Column(name = "email", nullable = false, length = 250)
        val email: String,

        @Column(name = "code", nullable = false)
        val code: Int = CodeGenerator.generate(),

        @Column(name = "shopId", nullable = false, length = 250)
        val shopId: String,

        @Column(name = "verified", nullable = false)
        val verified: Boolean = false,

        @Column(name = "windowId", nullable = false, length = 250)
        val windowId: String,

        @Column(name = "issuedAt", nullable = false)
        val issuedAt: Date,

        @Column(name = "secret", nullable = false, unique = true, length = 250)
        val secret: String = UUID.randomUUID().toString()
)