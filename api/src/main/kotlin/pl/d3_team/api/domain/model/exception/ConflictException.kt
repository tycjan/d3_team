package pl.d3_team.api.domain.model.exception

class ConflictException : Exception()