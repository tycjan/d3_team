package pl.d3_team.api.application.model.request

data class LoginRequest(
        val email: String,
        val password: String
)