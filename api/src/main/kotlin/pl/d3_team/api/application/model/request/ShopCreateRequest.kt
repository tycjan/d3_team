package pl.d3_team.api.application.model.request

data class ShopCreateRequest(
        val name: String,
        val address: String,
        val openFrom: Int,
        val openTo: Int,
        val shopInterval: Int,
        val maxClient: Int
)