package pl.d3_team.api.application.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import pl.d3_team.api.application.auth.admin
import pl.d3_team.api.application.model.request.ShopCreateRequest
import pl.d3_team.api.domain.model.exception.NoContentException
import pl.d3_team.api.domain.model.exception.NotFoundException
import pl.d3_team.api.domain.model.exception.UnauthorizedException
import pl.d3_team.api.domain.service.ShopService
import javax.servlet.http.HttpServletRequest
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/")
class ShopController {

    @Autowired
    private lateinit var shopService: ShopService

    @PostMapping("shop")
    fun postShop(
            @Valid @RequestBody req: ShopCreateRequest,
            request: HttpServletRequest
    ) : ResponseEntity<Any> {
        val admin = request.admin() ?: throw UnauthorizedException()

        return ResponseEntity(shopService.postShop(req, admin), HttpStatus.CREATED)
    }

    @GetMapping("shop")
    fun getShop(
            request: HttpServletRequest
    ) : ResponseEntity<Any> {
        val admin = request.admin() ?: throw UnauthorizedException()

        val shop = shopService.getShop(admin) ?: throw NotFoundException()

        return ResponseEntity.ok(shop)
    }

    @GetMapping("shops")
    fun getShops() : ResponseEntity<Any> {
        val shops = shopService.getAllShops()

        if (shops.isEmpty()) throw NoContentException()

        return ResponseEntity.ok(shops)
    }

    @GetMapping("shop/{id}")
    fun getShop(
            @PathVariable("id") id: String
    ) : ResponseEntity<Any> {
        val shop = shopService.getById(id) ?: throw NotFoundException()

        return ResponseEntity.ok(shop)
    }
}