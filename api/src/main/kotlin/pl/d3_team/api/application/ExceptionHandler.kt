package pl.d3_team.api.application

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import pl.d3_team.api.domain.model.exception.*

@ControllerAdvice
class ErrorHandler {

    @ExceptionHandler(NotFoundException::class)
    fun notFound(): ResponseEntity<Any> =
            ResponseEntity(HttpStatus.NOT_FOUND)

    @ExceptionHandler(UnauthorizedException::class)
    fun unauthorized(): ResponseEntity<Any> =
            ResponseEntity(HttpStatus.UNAUTHORIZED)

    @ExceptionHandler(BadRequestException::class)
    fun badRequest(): ResponseEntity<Any> =
            ResponseEntity(HttpStatus.BAD_REQUEST)

    @ExceptionHandler(ConflictException::class)
    fun conflict(): ResponseEntity<Any> =
            ResponseEntity(HttpStatus.CONFLICT)

    @ExceptionHandler(NoContentException::class)
    fun noContent(): ResponseEntity<Any> =
            ResponseEntity(HttpStatus.NO_CONTENT)
}