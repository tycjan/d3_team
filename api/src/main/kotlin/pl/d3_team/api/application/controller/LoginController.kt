package pl.d3_team.api.application.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import pl.d3_team.api.application.model.request.LoginRequest
import pl.d3_team.api.domain.service.LoginService
import javax.servlet.http.HttpServletRequest
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/")
class LoginController {

    @Autowired
    private lateinit var loginService: LoginService

    @PostMapping("login")
    fun login(
            @Valid @RequestBody req: LoginRequest,
            request: HttpServletRequest
    ) : ResponseEntity<Any> {
        val token = loginService.login(req.email, req.password)

        return ResponseEntity.ok(token)
    }
}