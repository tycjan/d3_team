package pl.d3_team.api.application.scheduler

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import pl.d3_team.api.domain.service.CleanupService
import pl.d3_team.api.domain.service.ReservationService

@Component
class ScheduledTasks {

    @Autowired
    private lateinit var reservationService: ReservationService
    @Autowired
    private lateinit var cleanerService: CleanupService

    @Scheduled(fixedRate = 15 * 60 * 1000)
    fun removeReservations() {
        reservationService.removeUnverifiedReservations()
    }

    @Scheduled(cron = "0 0 0 * * *")
    fun cleanUp() {
        cleanerService.cleanup()
    }
}