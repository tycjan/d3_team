package pl.d3_team.api.application.auth

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.web.servlet.HandlerInterceptor
import pl.d3_team.api.application.C
import pl.d3_team.api.domain.model.entity.Admin
import pl.d3_team.api.domain.service.LoginService
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class AuthorizationInterceptor : HandlerInterceptor {

    @Autowired
    private lateinit var loginService: LoginService

    override fun preHandle(
            request: HttpServletRequest,
            response: HttpServletResponse,
            handler: Any
    ): Boolean {
        request.getHeader(C.AUTH_HEADER)?.let { token ->
            loginService.getByAuthToken(token)?.let { admin ->
                request.setAttribute(C.ADMIN_KEY, admin)
            }
        }

        return super.preHandle(request, response, handler)
    }
}

fun HttpServletRequest.admin(): Admin? =
        getAttribute(C.ADMIN_KEY)?.let {
            it as Admin
        }
