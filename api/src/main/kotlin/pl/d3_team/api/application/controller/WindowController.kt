package pl.d3_team.api.application.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import pl.d3_team.api.application.model.response.WindowResponse
import pl.d3_team.api.domain.model.exception.NoContentException
import pl.d3_team.api.domain.model.exception.NotFoundException
import pl.d3_team.api.domain.service.WindowService

@CrossOrigin
@RestController
@RequestMapping("/v1/")
class WindowController {

    @Autowired
    private lateinit var windowService: WindowService

    @GetMapping("windows")
    fun getWindows(
            @RequestParam("shop_id") shopId: String
    ): ResponseEntity<Any> {
        val windows = windowService.getByShopIdAndFree(shopId)

        val response = windows.map { WindowResponse(it) }

        if (response.isEmpty()) throw NoContentException()

        return ResponseEntity.ok(response)
    }

    @GetMapping("window/{id}")
    fun getWindowById(
            @PathVariable("id") id: String
    ): ResponseEntity<Any> {
        val window = windowService.getById(id) ?: throw NotFoundException()

        return ResponseEntity.ok(window)
    }
}