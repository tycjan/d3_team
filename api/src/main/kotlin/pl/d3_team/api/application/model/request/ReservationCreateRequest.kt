package pl.d3_team.api.application.model.request

data class ReservationCreateRequest(
     val email: String,
     val windowId: String
)