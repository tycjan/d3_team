package pl.d3_team.api.application.model.response

import pl.d3_team.api.domain.model.entity.Window
import java.time.LocalTime

data class WindowResponse(
        val id: String,
        val from: LocalTime,
        val to: LocalTime,
        val shopId: String
) {
    constructor(window: Window) : this(window.id, window.from, window.to, window.shopId)
}