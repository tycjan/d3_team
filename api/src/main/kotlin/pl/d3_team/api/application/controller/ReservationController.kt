package pl.d3_team.api.application.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import pl.d3_team.api.application.auth.admin
import pl.d3_team.api.application.model.request.ReservationCreateRequest
import pl.d3_team.api.domain.model.exception.BadRequestException
import pl.d3_team.api.domain.model.exception.NotFoundException
import pl.d3_team.api.domain.model.exception.UnauthorizedException
import pl.d3_team.api.domain.service.ReservationService
import pl.d3_team.api.domain.service.ShopService
import java.net.URI
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/")
class ReservationController(
        @Value("\${mail.redirect.url}")
        private val mailRedirectUrl: String
) {

    @Autowired
    private lateinit var reservationService: ReservationService
    @Autowired
    private lateinit var shopService: ShopService

    @PostMapping("reservation")
    fun postReservation(
        @Valid @RequestBody req: ReservationCreateRequest
    ): ResponseEntity<Any> {
        val reservation = reservationService.createReservation(req)

        return ResponseEntity(reservation, HttpStatus.CREATED)
    }

    @GetMapping("reservation/{id}")
    fun getReservation(
            @PathVariable("id")id: String
    ): ResponseEntity<Any> {
        val reservation = reservationService.getById(id) ?: throw NotFoundException()

        return ResponseEntity.ok(reservation)
    }

    @GetMapping("reservation/verify/{secret}")
    fun verifyReservation(
            @PathVariable("secret") secret: String,
            httpServletResponse: HttpServletResponse
    ): ResponseEntity<Any> {
        val reservation = reservationService.verify(secret)

        val queryParams="/${reservation.id}"
        val uri = URI(mailRedirectUrl+queryParams)

        val httpHeaders = HttpHeaders()
        httpHeaders.location = uri

        return ResponseEntity(httpHeaders, HttpStatus.SEE_OTHER)
    }

    @GetMapping("reservation/code/{code}")
    fun getReservationByCode(
            @PathVariable("code") code: Int,
            request: HttpServletRequest
    ): ResponseEntity<Any> {
        val admin = request.admin() ?: throw UnauthorizedException()
        val shop = shopService.getShop(admin) ?: throw BadRequestException()

        val reservation = reservationService.getByShopAndCode(shop, code)

        return ResponseEntity.ok(reservation)
    }
}