# **D3_team - safe-shopping - service**

Spring Service using DomainDrivenDesign (in a HackYeah way)

Mail Sending Service - MailJet

**ENV - VARIABLES**
- `D3_DATABASE_URL` (//127.0.0.1:3306/d3service ...)
- `D3_DATABASE_USERNAME`(root)
- `D3_DATABASE_PASSWORD`
- `D3_SERVICE_PORT` (9999)
- `D3_MAIL_REDIRECT_URL`
- `D3_MAILJET_PUBLIC_KEY`
- `D3_MAILJET_PRIVATE_KEY`
- `D3_MAILJET_SENDER_MAIL`
- `D3_MAILJET_TEMPLATE_ID`
- `D3_LOG_LVL` (debug)

**DEPLOY**
- `chmod +x ./gradlew`
- `./gradlew build -x test` // nobody likes tests on hackathon :)
- `screen java -jar api-{version}.jar`

