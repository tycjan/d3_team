import React, {useState} from 'react';
import axios from 'axios';

export const Login = ({setToken}) => {
    const [email, setEmail] = useState(null);
    const [password, setPassword] = useState(null);

    const login = async () => {
        try {
            if (email !== null && password !== null) {
                const res = await axios.post("http://safe-shopping.online:9999/v1/login", {
                    email, password
                });
                const token = res.data;

                if (token !== undefined) {
                    localStorage.setItem('token', token);
                    setToken(token);
                }
            }
        } catch (e) {}
    };

    return (<div className="bg-gray-100 shadow-md rounded px-8 pt-6 pb-8 mb-4 flex flex-col">
        <div className="mb-4">
            <label className="block text-grey-darker text-sm font-bold mb-2" htmlFor="username">
                E-mail
            </label>
            <input
                onChange={(event) => setEmail(event.target.value)}
                className="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker" id="email"
                type="email" placeholder="email"/>
        </div>
        <div className="mb-6">
            <label className="block text-grey-darker text-sm font-bold mb-2" htmlFor="password">
                Password
            </label>
            <input
                onChange={(event) => setPassword(event.target.value)}
                className="shadow appearance-none border border-red rounded w-full py-2 px-3 text-grey-darker mb-3"
                id="password" type="password" placeholder="******************"/>
        </div>
        <div className="flex items-center justify-between">
            <button onClick={() => login()} className="bg-blue-500 hover:bg-blue-700 text-white py-2 px-4 rounded-full">
                Sign In
            </button>
        </div>
    </div>)
};
