import React, {useState} from "react";
import axios from 'axios';

export const AddCompany = ({setCompany}) => {
    const [name, setName] = useState(null);
    const [address, setAddress] = useState(null);
    const [openFrom, setOpenFrom] = useState(5);
    const [openTo, setOpenTo] = useState(12);
    const [shopInterval, setShopInterval] = useState(null);
    const [maxClient, setMaxClient] = useState(null);

    const create = async () => {
        const token = localStorage.getItem('token');

        if (token !== null) {
            if (name !== null && address !== null && shopInterval !== null && maxClient !== null ) {
                if ((shopInterval >= 10 && shopInterval <= 60) && (maxClient >=1 && maxClient <= 100)) {
                    const res = await axios.post("http://safe-shopping.online:9999/v1/shop", {
                        name,
                        address,
                        openFrom: parseInt(openFrom),
                        openTo: parseInt(openTo),
                        shopInterval: parseInt(shopInterval),
                        maxClient: parseInt(maxClient)
                    }, {headers: {Authorization: token}});

                    if (res.data !== undefined) {
                        setCompany(res.data)
                    }
                }
            }
        }
    };

    return (
        <form className="flex-auto">
            <div className="flex flex-wrap -mx-3 mb-6">
                <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                        Shop Name
                    </label>
                    <input onChange={(event) => setName(event.target.value)}
                        className="appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
                        type="text" placeholder="name"
                    />
                </div>
                <div className="w-full md:w-1/2 px-3">
                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
                          >
                        Shop Address
                    </label>
                    <input onChange={(event) => setAddress(event.target.value)}
                        className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                       type="text" placeholder="address"/>
                </div>
            </div>
            <div className="flex flex-wrap -mx-3 mb-6">
                <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
                           htmlFor="grid-first-name">
                        Open from
                    </label>
                    <div className="relative">
                        <select onChange={event => setOpenFrom(event.target.value)}
                            className="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                            id="grid-state">
                            <option>5</option>
                            <option>6</option>
                            <option>7</option>
                            <option>8</option>
                            <option>9</option>
                            <option>10</option>
                            <option>11</option>
                            <option>12</option>
                        </select>
                        <div
                            className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                            <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg"
                                 viewBox="0 0 20 20">
                                <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
                            </svg>
                        </div>
                    </div>
                </div>
                <div className="w-full md:w-1/2 px-3">
                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
                           htmlFor="grid-last-name">
                       Open to
                    </label>
                    <div className="relative">
                        <select onChange={event => setOpenTo(event.target.value)}
                                className="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                id="grid-state">
                            <option>13</option>
                            <option>14</option>
                            <option>15</option>
                            <option>16</option>
                            <option>17</option>
                            <option>18</option>
                            <option>19</option>
                            <option>20</option>
                            <option>21</option>
                            <option>22</option>
                            <option>23</option>
                        </select>
                        <div
                            className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                            <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg"
                                 viewBox="0 0 20 20">
                                <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
                            </svg>
                        </div>
                    </div>
            </div>
            </div>
            <div className="flex flex-wrap -mx-3 mb-6">
                <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
                           htmlFor="grid-first-name">
                        Client shopping time (minutes)
                    </label>
                    <input
                        onChange={event => setShopInterval(event.target.value)}
                        className="appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
                        type="number"  min='10' max='60' placeholder="Min 10 - Max 60"/>
                </div>
                <div className="w-full md:w-1/2 px-3">
                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
                           htmlFor="grid-last-name">
                        Max clients in one shopping interval
                    </label>
                    <input
                        onChange={event => setMaxClient(event.target.value)}
                        className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                        type="number"  min='1' max='100' placeholder="Min 1 - Max 100"/>
                </div>
                <button onClick={(e) => {
                    e.stopPropagation();
                    e.preventDefault();
                    create();
                }} className="mt-8 bg-transparent hover:bg-green-500 text-green-700 font-semibold hover:text-white py-2 px-4 border border-green-500 hover:border-transparent rounded-full">
                    Add shop
                </button>
            </div>
        </form>
    );
};
