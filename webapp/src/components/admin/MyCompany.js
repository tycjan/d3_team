import React, {useState} from "react";
import {SuccessAlert} from "../alerts/SuccessAlert";
import {ErrorAlert} from "../alerts/ErrorAlert";
import axios from 'axios';

export const MyCompany = ({company}) => {
    const [successAlert, setSuccessAlert] = useState({show: false, msg: ''});
    const [errorAlert, setErrorAlert] = useState({show: false, msg: ''});
    const [code, setCode] = useState(null);

    const token = localStorage.getItem('token');

    const verify = async () => {
        if (code !== null )
        axios.get(`http://safe-shopping.online:9999/v1/reservation/code/${code}`, {headers: {Authorization: token}})
            .then((res) => {
                setSuccessAlert({show: true, msg: `User ${res.data.email} have reservation`});
            })
            .catch(() => {
                setErrorAlert({show: true, msg: `User does not have reservation`});
            })
    };

    return (
        <div className="flex-auto">
            {errorAlert.show ?  <ErrorAlert info={errorAlert.msg} closeAlert={() => setErrorAlert({show: false, msq: ''})}/> : null}
            {successAlert.show ?  <SuccessAlert info={successAlert.msg} closeAlert={() => setSuccessAlert({show: false, msq: ''})}/> : null}
            <div className="mt-4 mb-4 border-r border-b border-l border-t border-gray-400 lg:border-l-400 lg:border-t-400 lg:border-gray-400 bg-white rounded-lg p-4 flex flex-col justify-between leading-normal">
                <div className="mb-4">
                    <div className="flex justify-between">
                        <div className="text-gray-900 font-bold text-xl mb-2">{company.name}</div>
                        <div className="text-gray-900 font-bold text-xl mb-2">{company.address}</div>
                    </div>
                    <p className="text-l text-gray-600 flex items-center">Open hours: {company.openFrom}:00 - {company.openTo}:00</p>
                    <p className="text-l text-gray-600 flex items-center">Max clients in shopping interval: {company.maxClient}</p>
                    <p className="text-l text-gray-600 flex items-center">Client shopping time: {company.shopInterval}</p>
                </div>
            </div>
            <div
                className="border-r border-b border-l border-t border-gray-400 lg:border-l-400 lg:border-t-400 lg:border-gray-400 bg-white rounded-lg p-4 flex flex-col justify-between leading-normal">
                <div className="mb-1">
                    <p className="text-sm text-gray-600 flex items-center">
                        <svg className="fill-current text-green-500 w-8 h-8 mr-2" xmlns="http://www.w3.org/2000/svg"
                             viewBox="0 0 20 20">
                            <path
                                d="M4 8V6a6 6 0 1 1 12 0v2h1a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2v-8c0-1.1.9-2 2-2h1zm5 6.73V17h2v-2.27a2 2 0 1 0-2 0zM7 6v2h6V6a3 3 0 0 0-6 0z"/>
                        </svg>
                        <input
                            onChange={e => setCode(e.target.value)}
                            className="ml-4 mr-4 bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-green-500"
                            id="inline-full-name" type="text" placeholder="Provide code to check"/>
                            <button onClick={() => {
                                setSuccessAlert({show: false, msg: ''});
                                setErrorAlert({show: false, msg: ''});
                                verify();
                            }} className='bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded-full pl-8 pr-8'>
                                check
                            </button>
                    </p>
                </div>
            </div>
        </div>);
};
