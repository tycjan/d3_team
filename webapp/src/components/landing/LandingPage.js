import React from 'react';
import img from './logo1.png';

export const LandingPage = () => {
    return (<div className='container flex flex-col text-center justify-center'>
        <img className='object-none' src={img}/>
            <h1 className='text-5xl text-blue-400'>Safe Shopping</h1>
            <p className='text-l text-gray-500'>Help stop the spread of coronavirus
                by shopping safely, Pick available date and receive a code that authorizes you to enter to the store, so as to avoid crowded queues.
            </p>
        <div className='text-center mt-8'>
            <a href='/shops' className="w-2/3 bg-green-500 hover:text-green-700 hover:bg-transparent font-semibold text-white py-2 px-4 border border-green-500  rounded-full">
                Find Shops
            </a>
        </div>

    </div>);
};
