import React, {useEffect, useState} from 'react';
import {Login} from "../../admin/Login";
import {MyCompany} from "../../admin/MyCompany";
import {AddCompany} from "../../admin/AddCompany";
import axios from "axios";

export const AdminContainer = () => {
    const [token, setToken] = useState(localStorage.getItem('token'));
    const [company, setCompany] = useState(null);

    useEffect(() => {
        if (token !== null) {
            axios.get("http://safe-shopping.online:9999/v1/shop", {headers: {Authorization: token}})
                .then(res => setCompany(res.data));
        }
    }, [token]);

    let component;

    if (token === null) {
        component = <Login setToken={setToken}/>
    }

    else if (company) {
        component = <MyCompany company={company}/>
    }

    else if (company === null) {
        component = <AddCompany setCompany={setCompany}/>
    }

    return <React.Fragment>
        {component}
    </React.Fragment>
};
