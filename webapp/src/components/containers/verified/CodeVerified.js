import React, {useEffect, useState} from "react";
import {useParams} from "react-router-dom";
import axios from 'axios'

export const CodeVerified = () => {
    const {reservationId} = useParams();
    const [reservation, setReservation] = useState({});
    const [window, setWindow] = useState({});
    const [shop, setShop] = useState({});
    const [reservationExpired, setReservationExpired] = useState(false);

    useEffect(() => {
        axios.get("http://api.safe-shopping.online:9999/v1/reservation/"+reservationId)
            .then((response) => response.data)
            .then((data) => {
                setReservation(data);
                axios.get("http://api.safe-shopping.online:9999/v1/shop/"+data.shopId)
                    .then((response) => response.data)
                    .then((data) => {
                        setShop(data);
                    })
                    .catch((error) => {
                    });

                axios.get("http://api.safe-shopping.online:9999/v1/window/"+data.windowId)
                    .then((response) => response.data)
                    .then((data) => {
                        setWindow(data);
                    })
                    .catch((error) => {
                    });
            })
            .catch((error) => {
                setReservationExpired(true);
            });
    }, []);

    return <div className="flex flex-auto justify-center items-center flex-col mt-0">
        {!reservationExpired ?
            <React.Fragment>
                <h1 className='text-2xl text-green-400 mt-0'>Your code: </h1>
                <h1 className="bg-gray-300 p-4 text-gray-600 text-5xl mt-0 rounded-full tracking-widest">{reservation.code}</h1>
                <div className="flex flex-row pt-3 justify-between ">
                    <h1 className='text-xl text-gray-500 mt-0 pr-4'>Reservation time:</h1>
                    <h1 className='text-xl text-gray-500 mt-0'>{window.from}-{window.to}</h1>
                </div>
                <div className="flex flex-row pt-3">
                    <h1 className='text-xl text-gray-500 mt-0 pr-4'>Shop:</h1>
                    <h1 className='text-xl text-gray-500 mt-0'>{shop.name + "   " + shop.address}</h1>
                </div>
            </React.Fragment> :
            <h1 className='text-2xl text-gray-500 mt-0'>Your reservation expired, or wasn't created</h1>
        }
    </div>
};
