import React from 'react';
import {ShopList} from "../../customer/ShopList";

export const CustomerContainer = () => {
    return <div className="flex flex-auto flex-col">
        <p className="text-center text-blue-500 text-xl pb-2">Available shops now</p>
        <ShopList/>
        </div>
};
