import React, {useEffect} from 'react';
import {Shop} from "./shops/Shop";
import Modal from 'react-modal';
import {Reservation} from "./reservationModal/Reservation";
import {EmptyList} from "./EmptyList";
import axios from 'axios'

Modal.setAppElement('#root');

export const ShopList = () => {
    const [modalIsOpen, setIsOpen] = React.useState(false);
    const [width, setWidth] = React.useState(null);
    const [shops, setShops] = React.useState([]);
    const [currentShop, setCurrentShop] = React.useState({});

    const customStyles = {
        content : {
            width                 : '80%',
            height                : '80%',
            top                   : '50%',
            left                  : '50%',
            right                 : 'auto',
            bottom                : 'auto',
            marginRight           : '-50%',
            transform             : 'translate(-50%, -50%)',
        }
    };

    useEffect(() => {
        if (width > 1700) {
            customStyles.content.width = '60%'
        }

        if (width < 450) {
            customStyles.content.width = '90%'
        }
        setWidth(window.innerWidth);
    });

    useEffect( () => {
        axios.get("http://api.safe-shopping.online:9999/v1/shops")
            .then((response) => response.data)
            .then((data) => {
                setShops(data);
            })
            .catch(() => {
            });
    }, []);

    return (
        <React.Fragment>
        <Modal
            isOpen={modalIsOpen}
            onRequestClose={() => setIsOpen(false)}
            style={customStyles}
            contentLabel="Example Modal"
        >
            <div className="">
                <Reservation
                    shop={currentShop}
                    closeModal={() => setIsOpen(false)}
                />
            </div>

        </Modal>

        <table className="table-auto">
            <thead>
            <tr>
                <th className="px-4 py-2">Shop</th>
                <th className="px-4 py-2">Open hours</th>
                <th className="px-4 py-2">Reserve</th>
            </tr>
            </thead>
            <tbody>
            {shops.length > 0 ? shops.map(shop => <Shop key={shop.id} shop={shop} openModal={() => {setCurrentShop(shop); setIsOpen(true)}}/>) : <EmptyList/>}
            </tbody>
        </table>
        </React.Fragment>
    );
};
