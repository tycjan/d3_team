import React from "react";

export const EmptyList = () => {
    return (
        <tr>
            <td className="px-4 py-2 text-center">
               -
            </td>
            <td className="px-4 py-2 text-center">-</td>
            <td className="text-center px-4 py-2">
                -
            </td>
        </tr>
    )
};
