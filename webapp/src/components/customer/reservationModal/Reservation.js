import React, {createRef, useEffect, useState} from "react";
import axios from 'axios'
import validator from 'validator';
import {ErrorAlert} from "../../alerts/ErrorAlert";
import {SuccessAlert} from "../../alerts/SuccessAlert";

export const Reservation = ({shop, closeModal}) => {
    const [successAlert, setSuccessAlert] = useState({show: false, msg: ''});
    const [errorAlert, setErrorAlert] = useState({show: false, msg: ''});
    const [windows, setWindows] = useState(null);
    const dropdown = createRef();
    const [email, setEmail] = useState(null);

    useEffect(() => {
        axios.get(`http://api.safe-shopping.online:9999/v1/windows?shop_id=${shop.id}`)
            .then((res) => {
                if (res.status !== 204) {
                    setWindows(res.data);
                }
            })
            .catch((error) => {});
    }, []);

    const createReservation = () => {
        if (validator.isEmail(email)) {
            axios.post("http://safe-shopping.online:9999/v1/reservation", {
                email,
                windowId: dropdown.current.value,
            }).then(() => {
                setSuccessAlert({show: true, msg: `Activation link has been sent to ${email}, remeber to confirm your reservation`})
            }).catch(() => {
                setErrorAlert({show: true, msg: `You can reserve only one visit per shop daily`})
            })
        }
    };

    return (
        <div className="container flex-auto text-center">
        <div className="flex items-center content-center">
            <p className="text-xl">Add reservation</p>
            <span className="absolute top-0 bottom-0 right-0 px-4 py-3">
                <svg onClick={closeModal} className="fill-current h-6 w-6 text-black" role="button" xmlns="http://www.w3.org/2000/svg"
                     viewBox="0 0 20 20"><title>Close</title><path
                    d="M14.348 14.849a1.2 1.2 0 0 1-1.697 0L10 11.819l-2.651 3.029a1.2 1.2 0 1 1-1.697-1.697l2.758-3.15-2.759-3.152a1.2 1.2 0 1 1 1.697-1.697L10 8.183l2.651-3.031a1.2 1.2 0 1 1 1.697 1.697l-2.758 3.152 2.758 3.15a1.2 1.2 0 0 1 0 1.698z"/></svg>
              </span>
        </div>

            <div className='flex-auto items-center content-center justify-center mx-auto'>
                <div className='content-center border-gray-400 border-b-2 border-t-0 border-r-0 border-l-0 pt-3 w-full'> </div>
                <div className="flex flex-wrap -mx-3 mb-6">
                    <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                    </div>
                </div>

                {errorAlert.show ?  <div className='pb-5'><ErrorAlert info={errorAlert.msg} closeAlert={() => setErrorAlert({show: false, msq: ''})}/></div> : null}
                {successAlert.show ?   <div className='pb-5'><SuccessAlert info={successAlert.msg} closeAlert={() => setSuccessAlert({show: false, msq: ''})}/></div> : null}
                <div className='flex justify-between pb-4'>
                    <div className='text-l pb-4'>
                        {shop.name}
                    </div>
                    <div className='text-l pb-4'>
                        {shop.address}
                    </div>
                    <div className='text-l pb-4'>
                        {`${shop.openFrom}:00 - ${shop.openTo}:00`}
                    </div>
                </div>

                {windows !== null ?
                <div className="flex flex-wrap -mx-3 mb-6">
                    <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                        <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
                               htmlFor="grid-first-name">
                            Available hours
                        </label>
                        <div className="relative">
                            <select
                                ref={dropdown}
                                className="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                id="windowId">
                                {windows.map(window =><option value={window.id}> {window.from} - {window.to} </option>)}
                            </select>
                            <div
                                className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                                <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg"
                                     viewBox="0 0 20 20">
                                    <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
                                </svg>
                            </div>
                        </div>
                    </div>
                    <div className="w-full md:w-1/2 px-3">
                        <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
                               htmlFor="grid-first-name">
                            Email
                        </label>
                        <input
                            onChange={e => setEmail(e.target.value)}
                            className="appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
                            type="email"  placeholder="your@email.com"/>
                    </div>
                    <div className="w-full px-3">
                        <button onClick={() => {
                            setSuccessAlert({show: false, msg: ''});
                            setErrorAlert({show: false, msg: ''});
                            createReservation();
                        }}
                                className="w-full mt-4 bg-transparent hover:bg-green-500 text-green-700 font-semibold hover:text-white py-2 px-4 border border-green-500 hover:border-transparent rounded-full">
                            Submit
                        </button>
                    </div>
                </div>
                    : <div>
                        <p className='text-gray-500'>Shop is now closed, no reservations available</p>
                    </div>}
            </div>

        </div>
    );
};
