import React from "react";

export const Shop = ({shop, openModal}) => {
    return (
        <tr>
            <td className="px-4 py-2 text-left">
                {`${shop.name}`} {`${shop.address}`}
            </td>
            <td className="px-4 py-2 text-center">
                {`${shop.openFrom}:00 - ${shop.openTo}:00`}
            </td>
            <td className="text-center px-4 py-2">
                <button onClick={openModal} className="bg-green-400 hover:bg-green-600 text-center text-white py-2 px-4 rounded-full" type="button">
                    Reserve
                </button>
            </td>
        </tr>
    );
};
