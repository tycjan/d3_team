import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";
import {AdminContainer} from "./components/containers/admin/AdminContainer";
import {CustomerContainer} from "./components/containers/customer/CustomerContainer";
import {CodeVerified} from "./components/containers/verified/CodeVerified";
import {LandingPage} from "./components/landing/LandingPage";

function App() {
    return (
        <React.Fragment>
        <nav className="mx-auto flex shadow-lg h-12">
            <ul className="flex items-center flex-auto justify-between flex-wrap">
                <li className="mr-12 pl-8">
                    <a href='/' className="text-2xl hover:text-green-500">Safe Shopping </a>
                </li>
                <li className="mr-12 ">
                    <a href="/admin" className="text-blue-500 hover:text-blue-800">Admin</a>
                </li>
            </ul>
        </nav>
        <div className="container mx-auto p-10">
        <Router>
            <Switch>
                <Route path="/verified/:reservationId">
                    <CodeVerified/>
                </Route>one
                <Route path="/admin">
                    <AdminContainer/>
                </Route>
                <Route path="/shops">
                    <CustomerContainer/>
                </Route>
                <Route path="/">
                    <LandingPage/>
                </Route>
            </Switch>
        </Router>
        </div>
        </React.Fragment>
    );
}

export default App;
